package be.kdg.java2.demo3jdbctemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo3jdbctemplateApplication implements CommandLineRunner {
    @Autowired
    private PersonRepository personRepository;

    public static void main(String[] args) {
        SpringApplication.run(Demo3jdbctemplateApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        personRepository.findByFirstName("JACK").forEach(System.out::println);
        System.out.println(personRepository.findById(1));
        personRepository.findByName("POTTER").forEach(System.out::println);
    }
}
