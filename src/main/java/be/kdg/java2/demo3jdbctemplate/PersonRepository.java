package be.kdg.java2.demo3jdbctemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonRepository{
    private static final Logger log = LoggerFactory.getLogger(PersonRepository.class);

    private JdbcTemplate jdbcTemplate;

    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Person> findByFirstName(String firstName){
        return jdbcTemplate.query("SELECT * FROM PERSONS WHERE FIRSTNAME = ?",
                (rs, rowNum) -> new Person(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("firstname")), firstName);
    }

    public Person findById(int id){
        return jdbcTemplate.queryForObject("SELECT * FROM PERSONS WHERE ID = ?",
                this::mapRow, id);
    }

    public List<Person> findByName(String name){
        return jdbcTemplate.query("SELECT * FROM PERSONS WHERE NAME = ?",
                new BeanPropertyRowMapper<>(Person.class), name);
    }

    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Person(rs.getInt("id"),
                rs.getString("name"),
                rs.getString("firstname"));
    }
}
