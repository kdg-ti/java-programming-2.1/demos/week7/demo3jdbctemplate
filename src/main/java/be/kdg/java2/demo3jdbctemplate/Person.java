package be.kdg.java2.demo3jdbctemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Person {
    private static final Logger log = LoggerFactory.getLogger(Person.class);

    private int id;
    private String name;
    private String firstName;

    //Needed for the BeanRowMapper
    public Person() {
    }

    public Person(String name, String firstName) {
        this.name = name;
        this.firstName = firstName;
    }

    public Person(int id, String name, String firstName) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                '}';
    }
}
